import joinClassHandler from "../helpers/joinClassHandler.js";

const classRoutes = (app, options, done) => {
  app.post("/joinClass/:id", joinClassHandler);
  //   app.post("/deleteUser/:id");
  done();
};

export default classRoutes;
