// import { getDb } from "./connection.js";

// const findOneDocument = async ({ collectionName, query, options }) => {
//   const db = await getDb();
//   return db.collection(collectionName).findOne(query, options);
// };

// const findDocuments = async ({ collectionName, query, options }) => {
//   const db = await getDb();
//   return db.collection(collectionName).find(query, options).toArray();
// };

// const countDocuments = async ({ collectionName, query }) => {
//   const db = await getDb();
//   return db.collection(collectionName).countDocuments(query);
// };

// const createDocument = async ({
//   collectionName,
//   documentData,
//   showNewDocument,
// }) => {
//   const db = await getDb();
//   const { insertedId } = await db
//     .collection(collectionName)
//     .insertOne(documentData);
//   if (showNewDocument) {
//     return findOneDocument({ collectionName, query: { _id: insertedId } });
//   }

//   return insertedId;
// };

// const updateOneDocument = async ({
//   collectionName,
//   query,
//   set,
//   inc,
//   addToSet,
//   pullAll,
//   push,
//   unset,
// }) => {
//   const db = await getDb();
//   const updateDoc = {
//     $set: set,
//     $inc: inc,
//     $addToSet: addToSet,
//     $pullAll: pullAll,
//     $push: push,
//     $unset: unset,
//   };
//   Object.keys(updateDoc).forEach(
//     (key) => updateDoc[key] === undefined && delete updateDoc[key]
//   );
//   const result = await db
//     .collection(collectionName)
//     .updateOne(query, updateDoc);
//   return result;
// };

// const updateManyDocuments = async ({
//   collectionName,
//   query,
//   set,
//   inc,
//   addToSet,
//   pullAll,
//   push,
//   unset,
// }) => {
//   const db = await getDb();
//   const updateDoc = {
//     $set: set,
//     $inc: inc,
//     $addToSet: addToSet,
//     $pullAll: pullAll,
//     $push: push,
//     $unset: unset,
//   };
//   Object.keys(updateDoc).forEach(
//     (key) => updateDoc[key] === undefined && delete updateDoc[key]
//   );

//   const result = await db
//     .collection(collectionName)
//     .updateMany(query, updateDoc);
//   return result;
// };

// const deleteOneDocument = async ({ collectionName, query }) => {
//   const db = await getDb();
//   const result = await db.collection(collectionName).deleteOne(query);
//   return result;
// };
// const deleteDocuments = async ({ collectionName, query }) => {
//   const db = await getDb();
//   const result = await db.collection(collectionName).deleteMany(query);
//   return result;
// };
// export {
//   findOneDocument,
//   findDocuments,
//   countDocuments,
//   createDocument,
//   updateOneDocument,
//   updateManyDocuments,
//   deleteOneDocument,
//   deleteDocuments,
// };
