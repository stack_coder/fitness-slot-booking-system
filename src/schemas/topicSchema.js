import Joi from "joi";

const topicSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().required(),
  subject_id: Joi.string().required(),
});

const validateTopicSchema = (schema) => {
  return topicSchema.validateAsync(schema);
};

export { topicSchema, validateTopicSchema };
