import Joi from "joi";

const subjectSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().required(),
});

const validateSubjectSchema = (schema) => {
  return subjectSchema.validateAsync(schema);
};

export { subjectSchema, validateSubjectSchema };
