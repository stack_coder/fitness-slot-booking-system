import Joi from "joi";

const noteSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().required(),
  topic_id: Joi.string().required(),
});

const validateNoteSchema = (schema) => {
  return noteSchema.validateAsync(schema);
};

export { noteSchema, validateNoteSchema };
