import { v4 } from 'uuid'

const getRandomId = () => {
  const id = v4()
  return id
}

export default getRandomId
