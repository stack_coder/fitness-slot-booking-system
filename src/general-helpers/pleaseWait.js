const pleaseWait = (ms = 5000) => new Promise((resolve) => setTimeout(resolve, ms))

export default pleaseWait
