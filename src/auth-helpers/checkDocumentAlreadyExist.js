import { findOneDocument } from "../database-helpers/apiHelpers.js";

const checkDocumentAlreadyExist = async ({ collectionName, data }) => {
  const checkData = await findOneDocument({
    collectionName: collectionName,
    query: { name: data },
  });
  if (checkData) {
    return true;
  }
  return false;
};

export default checkDocumentAlreadyExist;
