import joinClass from "./class/joinClass.js";

const joinClassHandler = async (req, reply) => {
  try {
    const { id } = req.params;
    const classBooked = await joinClass(id, req.body);
    reply.send(classBooked);
  } catch (error) {}
};

export default joinClassHandler;
