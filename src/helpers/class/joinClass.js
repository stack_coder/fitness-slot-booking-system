import { classes, users } from "../../database-helpers/dummyData.js";
import classTypeAndCapacity from "../../enums/Classes.js";
import status from "../../enums/Status.js";

const joinClass = async (id, body) => {
  try {
    const { classType } = body;
    const user = await users.find((item) => item.id === id);
    const checkUser = classes.find((item) => item.id === user.id);
    console.log({ classes });

    if (!checkUser) {
      const filteredClasses = classes.filter(
        (item) => item.classType === classType
      );
      const classCapacity = classTypeAndCapacity.find(
        (item) => item.type === classType
      );

      if (filteredClasses.length < classCapacity.capacity) {
        const ClassesApproved = {
          ...body,
          ...user,
          status: status[1],
        };
        const bookedClasses = classes.push(ClassesApproved);
        return bookedClasses;
      } else {
        const ClassesPending = {
          ...body,
          ...user,
          status: status[2],
        };
        const pendingClasses = classes.push(ClassesPending);
        return pendingClasses;
      }
    }
  } catch (error) {}
};

export default joinClass;
