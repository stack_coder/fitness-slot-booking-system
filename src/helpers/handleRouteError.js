const handleRouteError = async ({ reply, error }) => {
  const { status, message } = error
  reply.send({ status, message })
}

export default handleRouteError
