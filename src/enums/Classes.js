const classTypeAndCapacity = [
  {
    type: "yoga",
    capacity: 3,
  },
  { type: "gym", capacity: 4 },
  { type: "dance", capacity: 4 },
];

export default classTypeAndCapacity;
