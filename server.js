import { app } from "./src/database-helpers/connection.js";

import fastifyCors from "fastify-cors";
import ngrok from "ngrok";
import fastifyEnv from "fastify-env";
import classRoutes from "./src/routes/classRoutes.js";
ngrok.connect();

const schema = {
  type: "object",
  required: ["PORT"],
  properties: {
    PORT: {
      type: "string",
      default: 3000,
    },
  },
};
const options = {
  schema,
  dotenv: true,
  data: process.env,
};

app.register(fastifyEnv, options);
await app.after();
app.register(fastifyCors);

// Routes
app.register(classRoutes);
app.listen(8080, "0.0.0.0", (err, address) => {
  if (err) {
    console.error(err);
  }

  console.log(`Server listening on ${address}`);
});
